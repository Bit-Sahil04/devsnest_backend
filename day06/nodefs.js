const fs = require('fs');
const args = process.argv.slice(2);

const argHandler = (pArgs,numMaxArgs = -1, argError = "", callback) => { // helper function for handling args 
    
    if (pArgs.length > numMaxArgs && numMaxArgs != -1) { 
        console.log(argError);
        return;
    }
    else if (numMaxArgs == -1 ){
        for (let arg of pArgs){
            callback(arg);
        }
    }
    else{
        callback(...pArgs);
    }

};
const errHandler = (err) => { if (err) throw err; };


switch(args[0]){
    default:
        console.log(
            "\nnodejs command parser for sync file system ops\n"+
            "Available Commands are:\n"+
            "(C) mkdir {path} - create new dir \n "+
            "(C) mkfile {[path1, path2....pathN]} - create new file(s) \n"+
            "(R) rdfile {[path1, path2,.... pathN]} - read the file(s)}\n"+
            "(U) rename {path1} {path2} - rename existing file or move it to new path\n" + 
            "(D) rmdir {path} - delete an exist. dir\n" +   
            "(D) unlink {path} - delete an exist. file\n"  
        );
        break;
    
    case "mkdir":
        argHandler(args.slice(1), -1, "errmsg", (path)=>{ 
            fs.mkdirSync(path, errHandler });
        break;
    
    case "rmdir":
        argHandler(args.slice(1), 1, "errmsg", (path)=>{ 
            fs.rmdirSync(path, errHandler })
         });
        break;
    
    case "unlink":
        argHandler(args.slice(1), 1, "errmsg", (path)=>{ 
            fs.unlinkSync(path, errHandler }, )
            });
        break;
    
    case "rename":
        argHandler(args.slice(1), 2, "errmsg", (path1, path2)=>{ 
            fs.renameSync(path1, path2 , errHandler })
         });
        break;
    
    case "rdfile":
        argHandler(args.slice(1), -1, "errmsg", (path1, path2)=>{ 
            let i = fs.readFileSync(path1, errHandler });
            console.log(i.toString());
         });
        break;

    case "mkfile":
        argHandler(args.slice(1), 2, "errmsg", (path, content)=>{ 
            fs.writeFileSync(path, content, errHandler });
        break;
}