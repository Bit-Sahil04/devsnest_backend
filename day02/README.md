# PostgreSQL

```sql
CREATE USER username WITH PASSWORD 'password';
CREATE DATABASE databasename;
GRANT ALL/SELECT/INSERT/DELETE/UPDATE PRIVILEGES ON DATABASE databasename TO username;
-- \c databasename connect to database

CREATE TABLE tablename (
  ColumnName DATATYPE NOT NULL,
  ColumnName DATATYPE,
);

-- \d to display list of tables
-- \d tablename to display table properties

DROP TABLE tablename; -- [delete table]

-- \l all existing database can be seen
DROP DATABASE databasename; -- delete database
-- NOTE: first connect to different database then drop required database
-- \q to exit postgres
```

```sql
CREATE SCHEMA schemaname; -- almost like folder/namespace
CREATE TABLE schemaname.tablename; -- create table within schema
DROP SCHEMA schemaname CASCADE; -- to drop objects of schema and delete schema

INSERT INTO TABLENAME(COLUMNNAME1,COLUMNNAME2)
VALUES
  ('1','2'), -- row1 data
  ('3','4'); -- row2 data

SELECT * FROM TABLENAME; -- to see data in table
SELECT (30*2) AS colname; -- to get the calculated value under colname
SELECT COUNT(*) AS "colname" FROM TABLENAME; -- to see how many records we have in a table
SELECT COUNT(queryname) AS "colname" FROM TABLENAME; -- to see how many records we have of a particular query
SELECT CURRENT_TIMESTAMP; -- to see current time

SELECT * FROM TABLENAME WHERE CONDITION;
SELECT * FROM TABLENAME WHERE colname BETWEEN START AND END;
SELECT * FROM TABLENAME WHERE colname IN (START, END);
SELECT * FROM TABLENAME WHERE colname LIKE 'firstLetter%';
SELECT * FROM TABLENAME WHERE colname LIKE 'firstLetter__otherLetters';
SELECT * FROM TABLENAME WHERE colname::newDatatype; -- [:: to typecast]

UPDATE TABLENAME SET colname = value WHERE CONDITION; --  to update value
DELETE FROM TABLENAME WHERE CONDITION; -- to delete a row

-- while creating tables you can write GENERATED ALWAYS AS IDENTITY which will give data automatically if you don't provide data according to the sequence no. going on --

CONSTRAINT name -- [name not necessary, postgres can give any name if you don't]
FOREIGN KEY colname -- [we have here given the constrains]
REFERENCES TABLENAME(primarykey) -- specify the tablename to which you want to connect]
ON DELETE CASCADE; -- only keep the rows which is in both table
```
